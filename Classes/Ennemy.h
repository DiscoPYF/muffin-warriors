#ifndef _ENNEMY_H_
#define _ENNEMY_H_

#include	"GroundedCharacter.h"

class Ennemy : public GroundedCharacter
{
public:
	Ennemy();
	virtual ~Ennemy();
};

#endif // !_ENNEMY_H_