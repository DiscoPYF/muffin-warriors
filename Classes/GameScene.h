#ifndef _GAME_SCENE_H_
#define	_GAME_SCENE_H_

#include	"cocos2d.h"
#include	"GameConfig.h"
#include	"Player.h"
#include	"ui/CocosGUI.h"

class GameScene : public cocos2d::Layer
{
public:
	GameScene();
	virtual ~GameScene();

	static cocos2d::Scene* createScene();
	virtual bool init() override;
	CREATE_FUNC(GameScene);
	void update(float) override;

	// Keyboard events
	void onKeyPressed(cocos2d::EventKeyboard::KeyCode, cocos2d::Event*);
	void onKeyReleased(cocos2d::EventKeyboard::KeyCode, cocos2d::Event*);

	// Buttons touched events
	void onLeftButtonTouched(Ref*, cocos2d::ui::Widget::TouchEventType);
	void onRightButtonTouched(Ref*, cocos2d::ui::Widget::TouchEventType);
	void onUpButtonTouched(Ref*, cocos2d::ui::Widget::TouchEventType);

	//void onPauseCallback(); // Need to define the type of callback depending on the control used
	//void onEndCallback(); // Need to define the type of callback depending on the control used

protected:
	cocos2d::Size visibleSize;
	cocos2d::Vec2 visibleOrigin;

	// Platforms
	std::vector<cocos2d::Sprite*> platforms; // Level logic - 1st platform must be ground
	//std::vector<cocos2d::Sprite*> accessiblePlatforms; // In case to many problem with word node border, create borders around screen then select only the one a muffin can be random on.

	// Muffin
	cocos2d::Label* muffinLabel;
	cocos2d::Sprite* muffinIcon;
	cocos2d::Sprite* muffin;
	int currentMuffinPlatformIndex;
	void randMuffin();
	void onMuffinCaught();

	// Player
	Player *player;

	// Enemies
	std::vector<cocos2d::Sprite*> enemies; // Supposed of being of no need if using the bitmask collision filter and categories with physics. + Level logic

	// Generation
	virtual void initResources(); // Level logic
	void(GameScene::*initLevel)(void);
	void initPlayer();
	void initSingleLevel(); // Level logic
	void initHud();
	void initUserInteractionListeners();

	// Physics
#ifdef PHYSICS_ON
	cocos2d::PhysicsWorld* physicsWorld;
	void setPhysicsWorld(cocos2d::PhysicsWorld*);

	// Init the physical world boundaries and other properties
	void initPhysics();

	void initPlatformsPhysics(); // Level logic + assuming all platform have the same rectangular shape for every level
#endif // PHYSICS_ON
};

#endif // !_GAME_SCENE_H_
