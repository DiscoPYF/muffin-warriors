#include "GroundedCharacter.h"

USING_NS_CC;

GroundedCharacter::GroundedCharacter() : state(CharacterState::Walking), direction(CharacterDirection::Right), isGrounded(true), isMoving(false), isFalling(false), animation(nullptr)
{
}

GroundedCharacter::~GroundedCharacter()
{
	if (this->animation != nullptr)
		this->animation->release();
}

bool GroundedCharacter::getIsMoving() const
{
	return (this->isMoving);
}

void GroundedCharacter::setIsMoving(bool value)
{
	this->isMoving = value;
}

CharacterState GroundedCharacter::getState() const
{
	return (this->state);
}

void GroundedCharacter::setState(CharacterState value)
{
	this->state = value;
}

CharacterDirection GroundedCharacter::getDirection() const
{
	return (this->direction);
}

void GroundedCharacter::setDirection(CharacterDirection value)
{
	this->direction = value;
}

bool GroundedCharacter::getIsGrounded() const
{
	return (this->isGrounded);
}

void GroundedCharacter::setIsGrounded(bool value)
{
	this->isGrounded = value;
}

bool GroundedCharacter::getIsFalling() const
{
	return (this->isFalling);
}

void GroundedCharacter::setIsFalling(bool value)
{
	this->isFalling = value;
}

void GroundedCharacter::moveRight()
{
	this->direction = CharacterDirection::Right;
	this->isMoving = true;
	this->setFlippedX(true);

#ifdef PHYSICS_ON
	this->updatePhysicsMovement();
#endif // PHYSICS_ON
}

void GroundedCharacter::moveLeft()
{
	this->direction = CharacterDirection::Left;
	this->isMoving = true;
	this->setFlippedX(false);

#ifdef PHYSICS_ON
	this->updatePhysicsMovement();
#endif // PHYSICS_ON
}

// /!\ CAN BE OPTIMIZED /!\
// ONE ANIMATION FOR EACH TYPE LOADED AT INIT, THEN LOAD THE ANIMATION PROPERTY WITH THE RIGHT ONE AT EVERY CHANGE, SO JUST NEED TO STOP/PLAY ANIMATION INSTEAD OF RECREATE IT FOR EACH MOVEMENT CHANGED
void GroundedCharacter::updateAnimation(std::string const & animationName)
{
	if (this->animation != nullptr)
		this->animation->release();
	this->stopAllActions();
	this->animation = AnimationCache::getInstance()->getAnimation(animationName);
	this->animation->retain();
	this->runAction(RepeatForever::create(Animate::create(this->animation)));
}

#ifdef PHYSICS_ON

void GroundedCharacter::updatePhysicsBody()
{
	PhysicsBody* physicsBody;
	PhysicsBody* oldPhysicsBody;

	physicsBody = PhysicsBody::createBox(this->getContentSize(), PhysicsMaterial(0.0f, 0.0f, 0.0f));
	physicsBody->setRotationEnable(false);
	oldPhysicsBody = this->getPhysicsBody();
	if (oldPhysicsBody != nullptr)
	{
		// Copy
		physicsBody->setAngularDamping(oldPhysicsBody->getAngularDamping());
		physicsBody->setLinearDamping(oldPhysicsBody->getLinearDamping());
		physicsBody->setMass(oldPhysicsBody->getMass());
		physicsBody->setMoment(oldPhysicsBody->getMoment());
		physicsBody->setVelocity(oldPhysicsBody->getVelocity());
		physicsBody->setVelocityLimit(physicsBody->getVelocityLimit());
	}
	else
	{
		physicsBody->setAngularDamping(0.0f);
		physicsBody->setLinearDamping(0.0f);
		physicsBody->setVelocityLimit(PLAYER_SPEED);
	}
	this->setPhysicsBody(physicsBody);
}

#endif // PHYSICS_ON