#ifndef _PLAYER_H_
#define _PLAYER_H_

#include	"GroundedCharacter.h"
#include	"GameConfig.h"

#define PLAYER_INITIAL_VELOCITY_JUMP	850.0f
#define	PLAYER_JUMP_LENGTH				60.0f
#define	PLAYER_INITIAL_VELOCITY_WALK	500.0f
#define	PLAYER_FIRST_FRAME_NAME			"-idle (1).png"

enum Category
{
	Melee,
	Range,
	Instant
};

class SpriteFrameNameCategory
{
private:
	Category category;
	std::string characterName;

public:
	SpriteFrameNameCategory(std::string const & characterName, Category category) : characterName(characterName), category(category)
	{
	}

	~SpriteFrameNameCategory()
	{
	}

	Category getCategory() const
	{
		return (this->category);
	}

	std::string const & getCharacterName() const
	{
		return (this->characterName);
	}
};

class Player : public GroundedCharacter
{
protected:
#ifndef PHYSICS_ON

	float jumpLength;
	float initialJumpPosition;

#endif // !PHYSICS_ON

	int muffinCounter;

	// Character
	SpriteFrameNameCategory* currentCharacter;
	std::vector<SpriteFrameNameCategory*> characters;

public:
	Player();
	virtual ~Player();

	static Player* create();
	static Player* createWithInitialCharacter(std::string const &, Category);

	void addCharacter(std::string const &, Category);
	void changeCharacter();
	SpriteFrameNameCategory const * getCharacter() const;

#ifndef PHYSICS_ON

	float getJumpLength() const;
	void setJumpLength(float);
	float getInitialJumpPosition() const;
	void setInitialJumpPosition(float);

#endif // !PHYSICS_ON

	void jump();

	int getMuffinCounter() const;
	void upMuffingCounter();

	std::string const getAnimationNameByState() const override;

#ifdef PHYSICS_ON

	void updatePhysicsMovement() override;

#endif // PHYSICS_ON
};

#endif // !_PLAYER_H_