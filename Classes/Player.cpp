#include	"cocos2d.h"
#include	"Player.h"

USING_NS_CC;

Player::Player() : GroundedCharacter(), muffinCounter(0), characters()
{
}

Player::~Player()
{
	SpriteFrameNameCategory* character;

	for (int i = 0; i < (int)this->characters.size(); i++)
	{
		character = this->characters[i];
		delete character;
	}
}

Player* Player::create()
{
	Player *result;

	result = new (std::nothrow) Player();
	if (result && result->init())
	{
		result->autorelease();
		return (result);
	}
	CC_SAFE_DELETE(result);
	return (nullptr);
}

Player* Player::createWithInitialCharacter(std::string const & characterName, Category category)
{
	Player* result;
	std::string name;
	SpriteFrame* spriteFrame;

	result = new (std::nothrow) Player();
	if (result)
	{
		name = characterName + PLAYER_FIRST_FRAME_NAME;
		spriteFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(name);
		if (spriteFrame && result->initWithSpriteFrame(spriteFrame))
		{
			result->autorelease();
			result->addCharacter(characterName, category);
			result->currentCharacter = result->characters.back();
			name = result->getAnimationNameByState();
			if (!name.empty())
				result->updateAnimation(characterName + "-" + name);
			return (result);
		}
	}
	CC_SAFE_DELETE(result);
	return (nullptr);
}

void Player::addCharacter(std::string const & characterName, Category category)
{
	SpriteFrameNameCategory* character;

	character = new SpriteFrameNameCategory(characterName, category);
	this->characters.push_back(character);
}

void Player::changeCharacter()
{
	int selectedCharacterIndex;
	int size;
	std::string name;
	SpriteFrame* spriteFrame;
	Size lastSpriteSize;
	int newSpriteHeight;

	lastSpriteSize = this->getContentSize();
	size = (int)this->characters.size() - 1;
	do
	{
		selectedCharacterIndex = cocos2d::random(0, size);
	} while (this->characters[selectedCharacterIndex] == this->currentCharacter);
	this->currentCharacter = this->characters[selectedCharacterIndex];
	name = this->currentCharacter->getCharacterName() + PLAYER_FIRST_FRAME_NAME;
	spriteFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(name);
	if (spriteFrame)
		this->setSpriteFrame(spriteFrame);

	// Fixing position if sprite has been resized, to give the impression that it is still at the same place from feet, because enchor point is in the middle
	newSpriteHeight = this->getContentSize().height - lastSpriteSize.height;
	if (newSpriteHeight != 0)
		this->setPositionY(this->getPositionY() + newSpriteHeight / 2);

#ifdef PHYSICS_ON
	this->updatePhysicsBody();
#endif // PHYSIC_ON

	name = this->getAnimationNameByState();
	if (!name.empty())
		this->updateAnimation(this->currentCharacter->getCharacterName() + "-" + name);
}

SpriteFrameNameCategory const * Player::getCharacter() const
{
	return (this->currentCharacter);
}

#ifndef PHYSICS_ON

void Player::setJumpLength(float jumpLength) // NOT THE RIGHT WAY TO DO THIS
{
	this->jumpLength = jumpLength;
}

float Player::getJumpLength() const
{
	return (this->jumpLength);
}

float Player::getInitialJumpPosition() const
{
	return (this->initialJumpPosition);
}

void Player::setInitialJumpPosition(float value)
{
	this->initialJumpPosition = value;
}

#endif // !PHYSICS_ON

void Player::jump()
{
	//this->state = CharacterState::Jumping;
	//this->isGrounded = false;
	this->getPhysicsBody()->applyImpulse(Vect(0.0f, 200.0f));
}

int Player::getMuffinCounter() const
{
	return (this->muffinCounter);
}

void Player::upMuffingCounter()
{
	this->muffinCounter++;
}

std::string const Player::getAnimationNameByState() const
{
	CharacterState state;

	state = this->getState();
	if (state == CharacterState::Dying)
		return ("die");
	else if (state == CharacterState::Walking)
	{
		if (this->getIsMoving())
			return ("walk");
		else
			return ("idle");
	}
	return (std::string());
}

#ifdef PHYSICS_ON

void Player::updatePhysicsMovement()
{
	PhysicsBody* physicsBody;
	Vec2 velocity;

	physicsBody = this->getPhysicsBody();
	physicsBody->resetForces();
	if (this->isMoving)
	{
		if (this->direction == CharacterDirection::Left)
		{
			//physicsBody->setVelocity(Vect(-500.0f, 0.0f));
			//physicsBody->applyForce(Vect(-800.0f, 0.0f));
			physicsBody->applyImpulse(Vect(-PLAYER_SPEED, 0.0f));
			physicsBody->applyForce(Vect(-PLAYER_SPEED, 0.0f));
		}
		else if (this->direction == CharacterDirection::Right)
		{
			//physicsBody->setVelocity(Vect(500.0f, 0.0f));
			//physicsBody->applyForce(Vect(800.0f, 0.0f));
			physicsBody->applyImpulse(Vect(PLAYER_SPEED, 0.0f));
			physicsBody->applyForce(Vect(PLAYER_SPEED, 0.0f));
		}
	}
	else
	{
		velocity = physicsBody->getVelocity();
		velocity.x = 0.0f;
		physicsBody->setVelocity(velocity);
	}
}

#endif // PHYSICS_ON