#include	"GameScene.h"

USING_NS_CC;

GameScene::GameScene() : initLevel(&GameScene::initSingleLevel), currentMuffinPlatformIndex(-1)
{
}

GameScene::~GameScene()
{
}

#ifdef PHYSICS_ON

Scene* GameScene::createScene()
{
	Scene* scene;
	GameScene* layer;
	PhysicsWorld* physicsWorld;

	scene = Scene::createWithPhysics();
	physicsWorld = scene->getPhysicsWorld();
#ifdef PHYSICS_DEBUG_DRAW
	physicsWorld->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
#endif // PHYSIC_DEBUG_DRAW

	layer = GameScene::create(); // Calls init
	layer->setPhysicsWorld(physicsWorld);

	scene->addChild(layer);
	return (scene);
}

#else

Scene* KeyboardLayer::createScene()
{
	Scene* scene;
	GameScene* layer;

	scene = Scene::create();
	layer = GameScene::create();
	scene->addChild(layer);
	return (scene);
}

#endif // PHYSIC_ON

bool GameScene::init()
{
	Director* director;

	if (!Layer::init())
		return (false);

	director = Director::getInstance();
	this->visibleSize = director->getVisibleSize();
	this->visibleOrigin = director->getVisibleOrigin();

	this->initResources();

#ifdef PHYSICS_ON

	this->initPhysics();

#endif // PHYSICS_ON

	// init the level - platforms and ennemies
	(this->*initLevel)();

	// init player
	this->initPlayer();

	// rand muffin
	this->randMuffin();

	// Head-up display
	this->initHud();

	// User interaction
	this->initUserInteractionListeners();

	this->scheduleUpdate();

	return (true);
}

void GameScene::update(float delta)
{
	if (this->player->getBoundingBox().intersectsRect(this->muffin->getBoundingBox()))
		this->onMuffinCaught();
}

void GameScene::onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event)
{
	std::string name;

	switch (keyCode)
	{
	case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
		this->player->moveLeft();
		break;
	case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
		this->player->moveRight();
		break;
	default:
		break;
	}
	if (keyCode == EventKeyboard::KeyCode::KEY_SPACE && this->player->getState() != CharacterState::Jumping)
		this->player->jump();

	name = this->player->getAnimationNameByState();
	if (!name.empty())
		this->player->updateAnimation(this->player->getCharacter()->getCharacterName() + "-" + name);
}

void GameScene::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event)
{
	CharacterDirection direction;
	bool care;
	std::string name;

	care = true;
	if (keyCode == EventKeyboard::KeyCode::KEY_LEFT_ARROW)
		direction = CharacterDirection::Left;
	else if (keyCode == EventKeyboard::KeyCode::KEY_RIGHT_ARROW)
		direction = CharacterDirection::Right;
	else
		care = false;
	if (care && direction == this->player->getDirection())
	{
		this->player->setIsMoving(false);
		name = this->player->getAnimationNameByState();
		if (!name.empty())
			this->player->updateAnimation(this->player->getCharacter()->getCharacterName() + "-" + name);
#ifdef PHYSICS_ON
		this->player->updatePhysicsMovement();
#endif // PHYSICS_ON
	}
}

void GameScene::randMuffin()
{
	int platformsNumber;
	int selectedPlatformIndex;
	Sprite* platform;
	Vec2 position;

	platformsNumber = this->platforms.size();
	do
	{
		selectedPlatformIndex = cocos2d::random(0, platformsNumber - 1);
	} while (selectedPlatformIndex == this->currentMuffinPlatformIndex);
	platform = this->platforms[selectedPlatformIndex];
	position.x = cocos2d::random(platform->getPosition().x - platform->getContentSize().width / 2 + this->muffin->getContentSize().width / 2, platform->getPosition().x + platform->getContentSize().width / 2 - this->muffin->getContentSize().width / 2);
	position.y = platform->getPosition().y + platform->getContentSize().height / 2 + this->muffin->getContentSize().height / 2;
	this->muffin->setPosition(position);
}

void GameScene::onMuffinCaught()
{
	int muffinCounter;
	int value;
	int result;
	std::ostringstream os;

	this->player->upMuffingCounter();
	this->randMuffin();

	this->player->changeCharacter();

	// Update HUD
	muffinCounter = this->player->getMuffinCounter();
	os << muffinCounter;
	this->muffinLabel->setString(os.str());

	// Check for indicator length to add space between the muffin icon and the counter
	value = 10;
	do
	{
		result = muffinCounter - value;
		if (result == 0)
		{
			this->muffinIcon->setPositionX(this->muffinIcon->getPositionX() - this->muffinLabel->getContentSize().width / 2);
			this->muffinLabel->setPositionX(this->muffinLabel->getPositionX() - this->muffinLabel->getContentSize().width / 3);
		}
		value *= 10;
	} while (value <= muffinCounter);
}

void GameScene::initResources()
{
	SpriteFrameCache* spriteFrameCache;
	AnimationCache* animationCache;

	spriteFrameCache = SpriteFrameCache::getInstance();
	animationCache = AnimationCache::getInstance();

	spriteFrameCache->addSpriteFramesWithFile("windrider.plist");
	animationCache->addAnimationsWithFile("windrider-animations.plist");

	spriteFrameCache->addSpriteFramesWithFile("firebrand.plist");
	animationCache->addAnimationsWithFile("firebrand-animations.plist");

	spriteFrameCache->addSpriteFramesWithFile("nightshadow.plist");
	animationCache->addAnimationsWithFile("nightshadow-animations.plist");

	spriteFrameCache->addSpriteFramesWithFile("level.plist");
	spriteFrameCache->addSpriteFramesWithFile("box.plist");
	animationCache->addAnimationsWithFile("box-animations.plist");
}

void GameScene::initPlayer()
{
	this->player = Player::createWithInitialCharacter("windrider", Category::Range);
	this->player->addCharacter("firebrand", Category::Range);
	this->player->addCharacter("nightshadow", Category::Melee);
	this->player->setPosition(this->visibleSize.width / 2 + this->visibleOrigin.x, this->platforms[0]->getContentSize().height + this->visibleOrigin.y + this->player->getContentSize().height / 2);

	this->addChild(player);

#ifdef PHYSICS_ON

	this->player->updatePhysicsBody();

#endif // PHYSICS_ON

}

void GameScene::initSingleLevel()
{
	Sprite* ground;
	Sprite* middle;
	Sprite* sprite;
	float heightOfFirstFloor;

	// Background
	sprite = Sprite::createWithSpriteFrameName("background.png");
	sprite->setPosition(this->visibleSize.width / 2 + this->visibleOrigin.x, this->visibleSize.height / 2 + this->visibleOrigin.y);
	this->addChild(sprite);

	// Ground
	ground = Sprite::createWithSpriteFrameName("ground.png");
	ground->setPosition(this->visibleOrigin.x + ground->getContentSize().width / 2, this->visibleOrigin.y + ground->getContentSize().height / 2);
	this->addChild(ground);
	this->platforms.push_back(ground);

	// 1st floor
	heightOfFirstFloor = 200.0f;

	// Left
	sprite = Sprite::createWithSpriteFrameName("left.png");
	sprite->setPosition(this->visibleOrigin.x + sprite->getContentSize().width / 2, this->visibleOrigin.y + sprite->getContentSize().height / 2 + ground->getContentSize().height + heightOfFirstFloor);
	this->addChild(sprite);
	this->platforms.push_back(sprite);

	// Middle
	middle = Sprite::createWithSpriteFrameName("middle.png");
	middle->setPosition(this->visibleSize.width / 2 + this->visibleOrigin.x, this->visibleOrigin.y + middle->getContentSize().height / 2 + ground->getContentSize().height + heightOfFirstFloor);
	this->addChild(middle);
	this->platforms.push_back(middle);

	// Right
	sprite = Sprite::createWithSpriteFrameName("right.png");
	sprite->setPosition(this->visibleSize.width + this->visibleOrigin.x - sprite->getContentSize().width / 2, this->visibleOrigin.y + sprite->getContentSize().height / 2 + ground->getContentSize().height + heightOfFirstFloor);
	this->addChild(sprite);
	this->platforms.push_back(sprite);

	// 2nd floor

	// Left
	sprite = Sprite::createWithSpriteFrameName("left.png");
	sprite->setPosition(this->visibleSize.width / 2 + this->visibleOrigin.x - sprite->getContentSize().width / 2 - middle->getContentSize().width / 2, this->visibleOrigin.y + sprite->getContentSize().height / 2 + ground->getContentSize().height + heightOfFirstFloor * 2);
	this->addChild(sprite);
	this->platforms.push_back(sprite);

	// Right
	sprite = Sprite::createWithSpriteFrameName("right.png");
	sprite->setPosition(this->visibleSize.width / 2 + this->visibleOrigin.x + sprite->getContentSize().width / 2 + middle->getContentSize().width / 2, this->visibleOrigin.y + sprite->getContentSize().height / 2 + ground->getContentSize().height + heightOfFirstFloor * 2);
	this->addChild(sprite);
	this->platforms.push_back(sprite);

	// Muffin
	sprite = Sprite::createWithSpriteFrameName("box-idle (1).png");
	sprite->runAction(RepeatForever::create(Animate::create(AnimationCache::getInstance()->getAnimation("box-idle"))));
	this->muffin = sprite;
	this->addChild(sprite);

#ifdef PHYSICS_ON

	this->initPlatformsPhysics();

#endif // PHYSICS_ON

}

void GameScene::initHud()
{
	this->muffinLabel = Label::createWithSystemFont("0", "Arial", 40.0f);
	this->muffinLabel->setPosition(this->visibleSize.width + this->visibleOrigin.x - (this->muffinLabel->getContentSize().width / 2 + 20.0f), this->visibleSize.height + this->visibleOrigin.y - (this->muffinLabel->getContentSize().height / 2 + 12.0f));
	this->addChild(this->muffinLabel);

	this->muffinIcon = Sprite::createWithSpriteFrameName("box-idle (1).png");
	this->muffinIcon->setPosition(Vec2(this->visibleSize.width + this->visibleOrigin.x - (this->muffinLabel->getContentSize().width + this->muffinIcon->getContentSize().width / 2 + 20.0f), this->visibleSize.height + this->visibleOrigin.y - this->muffinIcon->getContentSize().height / 2 + 10.0f));
	this->addChild(this->muffinIcon);
}

void GameScene::initUserInteractionListeners()
{
	EventListenerKeyboard* eventListenerKeyboard;

	eventListenerKeyboard = EventListenerKeyboard::create();
	eventListenerKeyboard->onKeyPressed = CC_CALLBACK_2(GameScene::onKeyPressed, this);
	eventListenerKeyboard->onKeyReleased = CC_CALLBACK_2(GameScene::onKeyReleased, this);
	this->_eventDispatcher->addEventListenerWithSceneGraphPriority(eventListenerKeyboard, this);
}

#ifdef PHYSICS_ON

void GameScene::setPhysicsWorld(cocos2d::PhysicsWorld* world)
{
	this->physicsWorld = world;
}

void GameScene::initPhysics()
{
	PhysicsBody* physicsBody;
	Node* node;

	// This creates the limit of the physical world as a Node with a physical body
	physicsBody = PhysicsBody::createEdgeBox(this->visibleSize);
	node = Node::create();
	node->setPhysicsBody(physicsBody);
	node->setPosition(this->visibleSize.width / 2 + this->visibleOrigin.x, this->visibleSize.height / 2 + this->visibleOrigin.y);
	this->addChild(node);
}

void GameScene::initPlatformsPhysics()
{
	int size;
	Sprite* platform;
	PhysicsBody* physicsBody;

	size = this->platforms.size();
	for (int i = 0; i < size; i++)
	{
		platform = this->platforms[i];
		physicsBody = PhysicsBody::createBox(platform->getContentSize());
		physicsBody->setDynamic(false);
		platform->setPhysicsBody(physicsBody);
	}
}

#endif // PHYSICS_ON