#ifndef _GROUNDED_CHARACTER_H_
#define	_GROUNDED_CHARACTER_H_

#include	"cocos2d.h"
#include	"GameConfig.h"

enum CharacterState
{
	Walking,
	Jumping,
	Dying,
	None
};

enum CharacterDirection
{
	Right,
	Left
};

class GroundedCharacter : public cocos2d::Sprite
{
protected:
	CharacterState state;
	CharacterDirection direction;
	bool isMoving;
	bool isGrounded;
	bool isFalling;

	// Animation
	cocos2d::Animation* animation;

public:
	GroundedCharacter();
	virtual ~GroundedCharacter();

	bool getIsMoving() const;
	void setIsMoving(bool);

	CharacterState getState() const;
	void setState(CharacterState);

	CharacterDirection getDirection() const;
	void setDirection(CharacterDirection);

	bool getIsGrounded() const;
	void setIsGrounded(bool);

	bool getIsFalling() const;
	void setIsFalling(bool);

	virtual void moveRight();
	virtual void moveLeft();

	virtual std::string const getAnimationNameByState() const = 0;
	virtual void updateAnimation(std::string const &);

#ifdef PHYSICS_ON
	// Update the physics body attached to the character
	virtual void updatePhysicsBody();
	virtual void updatePhysicsMovement() = 0;
#endif // PHYSICS_ON
};

#endif // !_GROUNDED_CHARACTER_H_