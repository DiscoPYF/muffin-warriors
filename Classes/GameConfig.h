#ifndef _GAME_CONFIG_H_
#define	_GAME_CONFIG_H_

#define	START_LAYER		KeyboardLayer

// Screen
#define	GAME_WIDTH	1280
#define	GAME_HEIGHT	800

// Physics
#define	PHYSICS_ON
//#define	PHYSICS_DEBUG_DRAW

// Player
#define	PLAYER_SPEED	500.0f

#endif // !_GAME_CONFIG_H_